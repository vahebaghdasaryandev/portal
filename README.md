This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

* Copy `src/config.example.js` file to `src/config.js` and make your changes:
``
    apiUrl: 'http://localhost:8000'
``

* In the project directory, you can run:

### `npm install`

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

