import axios from 'axios';
import {apiUrl} from './config';

const axiosApi = axios.create({
    baseURL: apiUrl
});

axiosApi.interceptors.request.use(function (config) {
        const apiKey = localStorage.getItem('api_key');
        if (apiKey) {
            config.headers['x-api-key'] = `${apiKey}`;
        }
        return config;
    }, function (error) {
        return Promise.reject(error);
    }
);

export default axiosApi;
