import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {Button, Form} from 'react-bootstrap';
import {userInfo, getUserEmails, saveUserEmails} from './Services/DashboardService';
import AlertMessage from './AlertMessage';
import {logout} from "./Services/AuthService";

function Dashboard() {
    const [loggedUser, setLoggedUser] = useState(null);
    const [emails, setEmails] = useState("");
    const [alertMessage, setAlertMessage] = useState({});
    const history = useHistory();

    function onChangeEmails(e) {
        setEmails(e.target.value);
    }

    function onLogout(e) {
        e.preventDefault();
        logout().then(() => {
            localStorage.clear();
            setLoggedUser(null);
            history.push("login");
        }).catch(console.error);
    }

    function onSaveEmails(e) {
        e.preventDefault();
        setAlertMessage({loading: true});
        saveUserEmails(emails).then(resp => {
            if (resp) {
                const {status, message, data} = resp;
                if (status) {
                    setAlertMessage({variant: 'info', message: message});
                    setEmails(data);
                } else {
                    setAlertMessage({variant: 'danger', message: message});
                }
            } else {
                setAlertMessage({variant: 'danger', message: "Oops, something went wrong..."});
            }
        }).catch(e => {
            setAlertMessage({variant: 'danger', message: e.message});
        });
    }

    useEffect(() => {
        const user = localStorage.getItem('user');
        if (user) {
            setLoggedUser(JSON.parse(user));
        }

        const apiKey = localStorage.getItem('api_key');
        if (apiKey) {
            // checking is valid apiKey
            userInfo().then(resp => {
                if (resp) {
                    const {status, data} = resp;
                    if (!status) { // Invalid api key
                        history.push("login");
                    }
                    localStorage.setItem('user', JSON.stringify(data));
                    getUserEmails().then(r => {
                        if (r) setEmails(r.data);
                    }).catch(console.error);
                }
            }).catch(() => {
                localStorage.removeItem('api_key');
                localStorage.removeItem('user');
                history.push("login");
            });
        } else { // invalid api key
            history.push("login");
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <div>
            <h2>Dashboard page</h2>
            <div className="text-right">
                {loggedUser && (<span className="p-2">Primary Email: <b>{loggedUser.email}</b></span>)}
                <Button variant="primary" size="sm" onClick={(e) => onLogout(e)}>
                    Logout
                </Button>
            </div>
            <AlertMessage alertMessage={alertMessage}/>
            <Form>
                <Form.Group controlId="emailsTextarea">
                    <Form.Label>Emails (from file)</Form.Label>
                    <Form.Control as="textarea" rows="3" placeholder="Add email addresses separated by comma"
                                  onChange={e => onChangeEmails(e)} value={emails}/>
                </Form.Group>
                <Button variant="primary" type="submit" onClick={(e) => onSaveEmails(e)}>
                    Save Emails
                </Button>
            </Form>
        </div>
    );
}

export default Dashboard;

