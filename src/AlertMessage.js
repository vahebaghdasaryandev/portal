import React from 'react';
import {Alert, Spinner} from 'react-bootstrap';

function AlertMessage(props) {
    const {alertMessage} = props;

    return (
        <div>
            {
                (alertMessage.variant && (
                    <Alert key="alert-1" variant={alertMessage.variant}>
                        {alertMessage.message}
                    </Alert>
                ))
            }
            {alertMessage.loading && <Spinner animation="border"/>}
        </div>
    );
}

export default AlertMessage;

