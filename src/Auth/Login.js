import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import AlertMessage from '../AlertMessage';
import {sendLoginLink, verifyLogin} from '../Services/AuthService';

function Login() {
    const history = useHistory();
    const [email, setEmail] = useState(null);
    const [alertMessage, setAlertMessage] = useState({});

    function onChangeEmail(e) {
        setEmail(e.target.value);
    }
   
    useEffect(() => {
        const search = window.location.search;
        const params = new URLSearchParams(search);
        const _verifyLogin = params.get('verifyLogin');
        if (_verifyLogin) {
            verifyLogin(_verifyLogin).then(resp => {
                if (resp) {
                    const {status, data} = resp;
                    if (status && data) {
                        localStorage.setItem('api_key', data['login_uuid'] + '_' + data['auth_token']);
                        if (data['user']) {
                            localStorage.setItem('user', JSON.stringify(data['user']));
                        }
                        history.push("dashboard");
                    }
                }
            }).catch(e => {
                console.log(e.message);
            });
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps


    function onSendLoginLink(e) {
        e.preventDefault();
        setAlertMessage({loading: true});
        sendLoginLink(email).then(resp => {
            if (resp) {
                const {status, message} = resp;
                if (status) {
                    setAlertMessage({variant: 'info', message: message});
                } else {
                    setAlertMessage({variant: 'danger', message: message});
                }
            } else {
                setAlertMessage({variant: 'danger', message: "Oops, something went wrong..."});
            }
        }).catch(e => {
            setAlertMessage({variant: 'danger', message: e.message});
        });
    }

    return (
        <div>
            <h2>Login page</h2>
            <AlertMessage alertMessage={alertMessage}/>
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter your email" onChange={e => onChangeEmail(e)}/>
                </Form.Group>
                <Button variant="primary" type="submit" onClick={(e) => onSendLoginLink(e)}>
                    Send Login Link
                </Button>
            </Form>
        </div>
    );
}

export default Login;

