import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import {Container} from 'react-bootstrap';
import './App.css';
import Login from './Auth/Login';
import Dashboard from './Dashboard';


function App() {
    return (
        <Router>
            <Container>
                <Switch>
                    <Route path="/login">
                        <Login/>
                    </Route>
                    <Route path="/">
                        <Dashboard/>
                    </Route>
                </Switch>
            </Container>
        </Router>
    );
}

export default App;
