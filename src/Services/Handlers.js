import {ACCESS_DENIED_MESSAGE} from '../config';

export const successHandler = function (resp) {
    if (resp && resp.data) {
        const {status, message} = resp.data;
        if (status === false && message === ACCESS_DENIED_MESSAGE) {
            window.location.href = '/login';
            return;
        }
        return resp.data;
    }
    return null;
}

export const errorHandler = function (e) {
    console.log(e.message);
    return e;
}