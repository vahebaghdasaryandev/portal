import axiosApi from "../axiosApi";
import {apiUrl} from '../config';
import {successHandler, errorHandler} from './Handlers';

export const userInfo = function () {
    return axiosApi.get(`${apiUrl}/me`).then(successHandler).catch(errorHandler);
}

export const getUserEmails = function () {
    return axiosApi.get(`${apiUrl}/userEmails`).then(successHandler).catch(errorHandler);
}

export const saveUserEmails = function (emails) {
    return axiosApi.post(`${apiUrl}/saveUserEmails`, {emails: emails}).then(successHandler).catch(errorHandler);
}
