import axiosApi from '../axiosApi';
import {apiUrl} from '../config';
import {successHandler, errorHandler} from './Handlers';

export const sendLoginLink = function (email) {
    return axiosApi.post(`${apiUrl}/sendLoginLink`, {email: email}).then(successHandler).catch(errorHandler);
}

export const verifyLogin = function (verifyLogin) {
    return axiosApi.get(`${apiUrl}/verifyLogin/${verifyLogin}`).then(successHandler).catch(errorHandler);
}

export const logout = function () {
    return axiosApi.get(`${apiUrl}/logout`).then(successHandler).catch(errorHandler);
}